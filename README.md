# Avoindata_Projekti

## Author 
Jani Paavilainen

## Details
Grand Tourism-o is a program that can search events and places happening in divisions indside Helsinki. Users needs to register and account and log in to use the program.
User can set their home division to search events quickly happening in that area. It is also possible to search events and places with keywords like "taide" or "koulu",
which returns results associated to the keyword. The results are returned in finnish because the API always returns data as finnish and only sometimes in english or swedish.
User can also add event to his/her favorites which are stored in database. User can view these in his/her profile page.
If users moves to a different division he is able to change his/her home division in the profile page.

## Changes after demo
Created a relational database.
Added function to allow user to store favourites into separate table and view them in the profile page by a press of a button.

## Api call example

example url: "http://api.hel.fi/linkedevents/v1/place/?division=espoo"

```
{
    "meta": {
        "count": 1238,
        "next": "http://api.hel.fi/linkedevents/v1/place/?page=2",
        "previous": null
    },
    "data": [
        {
            "id": "tprek:15417",
            "divisions": [
                {
                    "type": "muni",
                    "ocd_id": "ocd-division/country:fi/kunta:espoo",
                    "municipality": null,
                    "name": {
                        "fi": "Espoo",
                        "sv": "Esbo"
                    }
                }
            ],
            "created_time": null,
            "last_modified_time": "2019-09-19T15:09:17.348722Z",
            "custom_data": null,
            "email": "sellonkirjasto@espoo.fi",
            "contact_type": null,
            "address_region": null,
            "postal_code": "02600",
            "post_office_box_num": null,
            "address_country": null,
            "deleted": false,
            "n_events": 25087,
            "image": 54259,
            "data_source": "tprek",
            "publisher": "ahjo:u021600",
            "parent": null,
            "replaced_by": null,
            "position": {
                "type": "Point",
                "coordinates": [
                    24.80992,
                    60.21748
                ]
            },
            "name": {
                "fi": "Sellon kirjasto",
                "sv": "Sellobiblioteket",
                "en": "Sellon kirjasto"
            },
            "description": null,
            "telephone": {
                "fi": "+358 9 8165 7603"
            },
            "address_locality": {
                "fi": "Espoo",
                "sv": "Esbo",
                "en": "Espoo"
            },
            "street_address": {
                "fi": "Leppävaarankatu 9",
                "sv": "Albergagatan 9",
                "en": "Leppävaarankatu 9"
            },
            "info_url": {
                "fi": "http://www.helmet.fi/sello",
                "sv": "http://www.helmet.fi/sello",
                "en": "http://www.helmet.fi/sello"
            },
            "@id": "http://api.hel.fi/linkedevents/v1/place/tprek:15417/",
            "@context": "http://schema.org",
            "@type": "Place"
        }
        ```
